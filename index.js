const express = require('express');
const app = express();
const dotenv = require('dotenv');
const expressLayouts = require('express-ejs-layouts');
const mysql = require('mysql');
const flash = require('connect-flash');
const session = require('express-session');
var path = require('path');

dotenv.config();
// EJS
app.use(expressLayouts);
app.set('view engine','ejs');

//Bodyparsers
app.use(express.urlencoded({ extended: false}))

app.use(express.json());

app.use(express.static(path.join(__dirname, 'public')));
/*
app.use(flash());
// Express session
app.use(
    session({
      secret: 'secret',
      resave: true,
      saveUninitialized: true
    })
  );
  

//var globals
app.use(function(req, res, next) {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    next();
  });
*/
 //Routes
 app.use('/' , require('./routes/welcome')); 
 app.use('/' , require('./routes/auth'));
 app.use('/' , require('./routes/tweet'));
 app.use('/' , require('./routes/comment'));

 
app.listen(5000, () => console.log("server running"));