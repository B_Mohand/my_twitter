const mysql = require('mysql');
const util = require('util');
const pool = mysql.createPool({
    host : 'localhost',
    user : 'root',
    password : '',
    database : 'tweeter_data',
});

pool.getConnection((err, connetion) =>{
    if (err)
        console.error("connetion failed")
    if(connetion)
    connetion.release();
    return;
})

pool.query = util.promisify(pool.query);

module.exports = pool;