const jwt = require('jsonwebtoken');

module.exports = function use (req,res,next){
    const token = localStorage.getItem('myToken');
    console.log(token)
    jwt.verify(token,process.env.TOKEN_SECRET), function(err, decodedToken) {
        if(err) {throw (err) }
        else {
         req.userId = decodedToken.id;  
         next();
        }
      };
     };

