const pool = require('../db.config');
var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');

function User(){};

User.prototype = {
    find : function(email = null,callback)
    {
        let sql = 'SELECT * FROM user where email = ?';
        pool.query(sql,email,function(err,result){
            if(err) throw err;
            return callback(result);
        });
    },

    create : (data, callback) =>
    {
        let pwd = data.password;
        data.password = bcrypt.hashSync(pwd,10)

        let sql = 'INSERT INTO user (username ,email ,password) VALUES (?, ?, ?)'
        pool.query(sql,[data.username, data.email , data.password],function(err, res){    
            if(err){
            return  callback(err);
            }
            return callback(null,res);
        });
    },
   /* 
    login : function(email,password, result)
    {
        console.log(email);
        console.log(password);
        this.find(email, function(res){
            console.log(res);
            len = Object.keys(res).length 
            if (len > 0) {
                x = bcrypt.compare(password, res[0].password, function(err, reslt) {
                    if(err) {
                        console.log("error: ", err);
                        result(err, null);
                    }
                    else{
                        result(null, reslt);             
                    }
            }) 
            }
        })
    }*/
    login : function(email,password, result){
        this.find(email, function(res,err){
            x = Object.keys(res).length;
            if (x > 0) {
                x = bcrypt.compare(password, res[0].password, function(err, reslt) {
                    if(err) {
                        console.log("error: ", err);
                        result(err, null);
                    }
                    else{
                        result(null, reslt);
                        var token = jwt.sign({userId : res[0].id, username : res[0].username, email : res[0].email},process.env.TOKEN_SECRET,{
                            expiresIn: 86400  
                        }); 
                        localStorage.setItem('myToken', token);
                    }
            }) 
            } else {
               result(err, null);
            }
        })
    }
}
module.exports = User;

