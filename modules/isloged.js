const jwt = require('jsonwebtoken');
var User = require('../modules/User');
const db = require('../db.config');
const user = new User();

module.exports = function isloged (req,res,next){
    const token = localStorage.getItem('myToken');
    let errors = [];
    if(!token) {
        next();
    }
    else{
    try{
        const decoded = jwt.verify(token,process.env.TOKEN_SECRET);
        errors.push({ msg: 'you are already loged in'});
        let sql = "SELECT * FROM tweets WHERE user_ID = (?)";
        user.find(decoded.email, function(info,error){
            if (error) {
        throw (error)
        } else {
            db.query(sql,[info[0].id],function(err,result){
                res.render('dashboard',{ errors, tweets:result , username: info[0].username });
             });
        }
        });
    }catch(err){
        console.log(err);
        errors.push({ msg: 'Invalid Token'});
        res.render("login",{errors});
    }
}
}
