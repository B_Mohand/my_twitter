const jwt = require('jsonwebtoken');

module.exports = function auth (req,res,next){
    const token = localStorage.getItem('myToken');
    let errors = [];
    if(!token) {
        errors.push({ msg: 'Access Denied u have to login'});
        res.render("login",{errors});
    }
    else{
    try{
        const verified = jwt.verify(token,process.env.TOKEN_SECRET);
        req.user = verified;
        next();
    }catch(err){
        errors.push({ msg: 'Invalid Token'});
        res.render("login",{errors});
    }
}
}
