var router = require('express').Router();
var User = require('../modules/User');
var verify = require('../modules/authToken');
var jwt = require('jsonwebtoken');
var checklog = require('../modules/isloged');

const user = new User();

if (typeof localStorage === "undefined" || localStorage === null) {
    const LocalStorage = require('node-localstorage').LocalStorage;
    localStorage = new LocalStorage('./scratch');
  }


router.get('/login',checklog,(req ,res) => res.render('login'));

router.get('/register',checklog,(req ,res) => res.render('register'));


router.post('/login', (req,res)=> {
    
    user.login(req.body.email, req.body.password,function(err,result){
        let errors = [];
        if(result){
            res.redirect('/home');
        }else{
        errors.push({ msg: 'Email or Password is incorect'});
        res.render('login',{errors})
        }
    });
});
 

router.post('/register',(req, res)=> {

    const {name, email, password, password2}= req.body;
    let errors = [];

    if(!name || !email || !password |[ !password2]){
        errors.push({ msg: 'Please fill in all fields'});
    }

    if(password !== password2){
        errors.push({ msg: 'Passwords do not match'});
    }
    if(password.length < 6){
        errors.push({ msg: 'Password should be at least 6 characters'});
    }

    if (errors.length > 0){
        res.render('register',{
            errors,
            name,
            email,
            password,
            password2,
        })
    }
 else { 
    user.find(email, function(result,err){
        x = Object.keys(result).length;
        if (x > 0) {
            if(email === result[0].email){
                errors.push({ msg: 'The email is already registered'});
                res.render('register',{
                    errors,
                    name,
                    email,
                    password,
                    password2,
                })
            } else {
                let inputUser = {
                    username : req.body.name,
                    email : req.body.email,
                    password : req.body.password
                };
                user.create(inputUser,function(err ,rows){
                    if(err){
                        throw err;
                    } else{
                    res.redirect('/login');
                    }
                });
            }
    } else {
    let inputUser = {
        username : req.body.name,
        email : req.body.email,
        password : req.body.password
    };
    user.create(inputUser,function(err ,rows){
        if(err){
            throw err;
        } else{
        res.redirect('/login');
        }
    });
    }
  });
}
});

router.get('/logout',(req, res)=> {
    localStorage.removeItem('myToken');
    res.redirect('login');
});
 
module.exports = router;