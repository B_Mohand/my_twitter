var router = require('express').Router();
var verify = require('../modules/authToken')
const db = require('../db.config');
var User = require('../modules/User');
var usetoken = require('../modules/useToken')
const user = new User();
const token = localStorage.getItem('myToken');

router.get('/home',verify,function(req,res){
    var decoded = req.user;
    console.log(decoded)
    let sql = "SELECT * FROM tweets WHERE user_ID = (?)";
    user.find(decoded.email, function(info,err){
        if (err) {
    throw (err)
    } else {
        db.query(sql,[info[0].id],function(err,result){
            res.render('dashboard',{ tweets:result , username: info[0].username });
         });
    }
    });
});

router.get('/tweet',verify,function(req,res){
    var decoded = req.user;
    let sql = "SELECT user.username as user , tweets.content as tweet , tweets.id as id  FROM tweets  join user on tweets.user_ID = user.id ";
    db.query(sql,function(err,result){
            res.render('blog',{ tweets:result , username:decoded.username });   
         });
        });

router.get('/post',verify,function(req,res){
    res.render('post');
});

router.post('/post',verify,function(req,res){
    var decoded = req.user;
    let sql = "INSERT INTO tweets (content , user_ID) VALUES (?,?)";
    user.find(decoded.email, function(info,err){
        if (err) {
    throw (err)
    } else {
        db.query(sql,[req.body.tweet,info[0].id],function(err,result){
            if(err) throw err;
            res.redirect('/tweet');
        });
    }
    });

});

 router.get('/delete/:id',verify,function(req,res){
    db.query('DELETE FROM tweets WHERE ID = ?',req.params.id,function(err,rs){
        res.redirect('/tweet');
    })
});

router.get('/tweet/edit/:id',verify,function(req,res){
    db.query("SELECT * FROM tweets where ID = ?",req.params.id,function(err,result){
        console.log(result);
        res.render('edit-tweet',{ tweet:result[0].content , id:req.params.id });  
     });  
});
router.get('/edit/:id',verify, function(req,res){
   let sql = "UPDATE tweets SET content = ? WHERE ID = ?";
   console.log(req.params.id);
    db.query(sql,[req.body.tweet,req.params.id],function(err,result){
        if(err) throw err;
        res.redirect('/tweet');
    });
})

module.exports =router; 