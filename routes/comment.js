var router = require('express').Router();
var verify = require('../modules/authToken')
const db = require('../db.config');
var User = require('../modules/User');
var usetoken = require('../modules/useToken')
const user = new User();
const token = localStorage.getItem('myToken');


router.get('/comments/:id',verify,function(req,res){
    let sql = "SELECT username as user , comments.content as comment FROM comments join user on comments.user_ID = user.id where tweet_id =(?) "
    db.query(sql,[req.params.id],function(err,result){
        console.log(result);
        res.render('comment',{comment:result});
    })
})

router.post('/postComment/:id',verify,function(req,res){
    var decoded = req.user;
    let sql = "INSERT INTO comments (content ,tweet_id,user_id) VALUES (?,?,?)";
    user.find(decoded.email, function(info,err){
        if (err) {
    throw (err)
    } else {
        db.query(sql,[req.body.comment,req.params.id,info[0].id],function(err,result){
            if(err) throw err;
            res.redirect('/tweet');
        });
    }
    });

});

 router.get('/delete/:id',verify,function(req,res){
    db.query('DELETE FROM tweets WHERE ID = ?',req.params.id,function(err,rs){
        res.redirect('/tweet');
    })
});

router.get('/tweet/edit/:id',verify,function(req,res){
    db.query("SELECT * FROM tweets where ID = ?",req.params.id,function(err,result){
        console.log(result);
        res.render('edit-tweet',{ tweet:result[0].content , id:result[0].ID});    
     });
     
});
router.post('/edit/:id',verify, function(req,res){
   let sql = "UPDATE tweets SET content = ? WHERE ID = ?";
    db.query(sql,[req.body.tweet,req.params.id],function(err,result){
        if(err) throw err;
        res.redirect('/tweet');
    });
   
})

module.exports =router; 