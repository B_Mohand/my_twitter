CREATE TABLE `tweet_likes` (
  `tweet_id` int,
  `user_id` int
);

CREATE TABLE `comment` (
  `comment_id` int,
  `tweet_id` int,
  `content` text,
  `user_id` int,
  `timestamp` timestamp,
  PRIMARY KEY (`comment_id`)
);

CREATE TABLE `followers` (
  `follower_id` int,
  `user_id` int,
  PRIMARY KEY (`follower_id`)
);

CREATE TABLE `user` (
  `user_id` int,
  `username` varchar,
  `password` varchar,
  `email` varchar,
  `phone_number` varchar,
  `adresse` varchar,
  `gender` varchar,
  `date of birth` varchar,
  `total following` int,
  `total followers` int,
  PRIMARY KEY (`user_id`)
);

CREATE TABLE `comment_likes` (
  `comment_id` int,
  `user_id` int
);

CREATE TABLE `tweets` (
  `tweet_id` int,
  `user_id` int,
  `content` text,
  `timestamp` timestamp,
  PRIMARY KEY (`tweet_id`)
);

